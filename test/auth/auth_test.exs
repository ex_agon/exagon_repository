defmodule Exagon.Repository.AuthTest do
  use Exagon.Repository.DataCase
  use Exagon.Repository.Schema

  alias Exagon.Repository.Auth
  alias Exagon.Repository.Repo

  import Exagon.Repository.AuthFixtures
  import Exagon.Repository.Factory
  alias Exagon.Repository.Auth.{User, UserToken, Workspace}

  describe "get_user_by_email/1" do
    test "does not return the user if the email does not exist" do
      refute Auth.get_user_by_email("unknown@example.com")
    end

    test "returns the user if the email exists" do
      %{id: id} = user = user_fixture()
      assert %User{id: ^id} = Auth.get_user_by_email(user.email)
    end
  end

  describe "get_user_by_email_and_password/2" do
    test "does not return the user if the email does not exist" do
      refute Auth.get_user_by_email_and_password("unknown@example.com", "hello world!")
    end

    test "does not return the user if the password is not valid" do
      user = user_fixture()
      refute Auth.get_user_by_email_and_password(user.email, "invalid")
    end

    test "returns the user if the email and password are valid" do
      %{id: id} = user = user_fixture()

      assert %User{id: ^id} =
               Auth.get_user_by_email_and_password(user.email, valid_user_password())
    end
  end

  describe "get_user!/1" do
    test "raises if id is invalid" do
      assert_raise Ecto.NoResultsError, fn ->
        Auth.get_user!(Ecto.UUID.generate())
      end
    end

    test "returns the user with the given id" do
      %{id: id} = user = user_fixture()
      assert %User{id: ^id} = Auth.get_user!(user.id)
    end
  end

  describe "register_user/1" do
    test "requires email and password to be set" do
      {:error, changeset} = Auth.register_user(%{})

      assert %{
               password: ["can't be blank"],
               email: ["can't be blank"]
             } = errors_on(changeset)
    end

    test "validates email and password when given" do
      {:error, changeset} = Auth.register_user(%{email: "not valid", password: "not valid"})

      assert %{
               email: ["must have the @ sign and no spaces"],
               password: ["should be at least 12 character(s)"]
             } = errors_on(changeset)
    end

    test "validates maximum values for email and password for security" do
      too_long = String.duplicate("db", 100)
      {:error, changeset} = Auth.register_user(%{email: too_long, password: too_long})
      assert "should be at most 160 character(s)" in errors_on(changeset).email
      assert "should be at most 72 character(s)" in errors_on(changeset).password
    end

    test "validates email uniqueness" do
      %{email: email} = user_fixture()
      {:error, changeset} = Auth.register_user(%{email: email})
      assert "has already been taken" in errors_on(changeset).email

      # Now try with the upper cased email too, to check that email case is ignored.
      {:error, changeset} = Auth.register_user(%{email: String.upcase(email)})
      assert "has already been taken" in errors_on(changeset).email
    end

    test "registers users with a hashed password" do
      email = unique_user_email()
      {:ok, user} = Auth.register_user(valid_user_attributes(email: email))
      assert user.email == email
      assert is_binary(user.hashed_password)
      assert is_nil(user.confirmed_at)
      assert is_nil(user.password)
    end
  end

  describe "change_user_registration/2" do
    test "returns a changeset" do
      assert %Ecto.Changeset{} = changeset = Auth.change_user_registration(%User{})
      assert changeset.required == [:password, :email]
    end

    test "allows fields to be set" do
      email = unique_user_email()
      password = valid_user_password()

      changeset =
        Auth.change_user_registration(
          %User{},
          valid_user_attributes(email: email, password: password)
        )

      assert changeset.valid?
      assert get_change(changeset, :email) == email
      assert get_change(changeset, :password) == password
      assert is_nil(get_change(changeset, :hashed_password))
    end
  end

  describe "change_user_email/2" do
    test "returns a user changeset" do
      assert %Ecto.Changeset{} = changeset = Auth.change_user_email(%User{})
      assert changeset.required == [:email]
    end
  end

  describe "apply_user_email/3" do
    setup do
      %{user: user_fixture()}
    end

    test "requires email to change", %{user: user} do
      {:error, changeset} = Auth.apply_user_email(user, valid_user_password(), %{})
      assert %{email: ["did not change"]} = errors_on(changeset)
    end

    test "validates email", %{user: user} do
      {:error, changeset} =
        Auth.apply_user_email(user, valid_user_password(), %{email: "not valid"})

      assert %{email: ["must have the @ sign and no spaces"]} = errors_on(changeset)
    end

    test "validates maximum value for email for security", %{user: user} do
      too_long = String.duplicate("db", 100)

      {:error, changeset} = Auth.apply_user_email(user, valid_user_password(), %{email: too_long})

      assert "should be at most 160 character(s)" in errors_on(changeset).email
    end

    test "validates email uniqueness", %{user: user} do
      %{email: email} = user_fixture()
      password = valid_user_password()

      {:error, changeset} = Auth.apply_user_email(user, password, %{email: email})

      assert "has already been taken" in errors_on(changeset).email
    end

    test "validates current password", %{user: user} do
      {:error, changeset} = Auth.apply_user_email(user, "invalid", %{email: unique_user_email()})

      assert %{current_password: ["is not valid"]} = errors_on(changeset)
    end

    test "applies the email without persisting it", %{user: user} do
      email = unique_user_email()
      {:ok, user} = Auth.apply_user_email(user, valid_user_password(), %{email: email})
      assert user.email == email
      assert Auth.get_user!(user.id).email != email
    end
  end

  describe "change_user_password/2" do
    test "returns a user changeset" do
      assert %Ecto.Changeset{} = changeset = Auth.change_user_password(%User{})
      assert changeset.required == [:password]
    end

    test "allows fields to be set" do
      changeset =
        Auth.change_user_password(%User{}, %{
          "password" => "new valid password"
        })

      assert changeset.valid?
      assert get_change(changeset, :password) == "new valid password"
      assert is_nil(get_change(changeset, :hashed_password))
    end
  end

  describe "update_user_password/3" do
    setup do
      %{user: user_fixture()}
    end

    test "validates password", %{user: user} do
      {:error, changeset} =
        Auth.update_user_password(user, valid_user_password(), %{
          password: "not valid",
          password_confirmation: "another"
        })

      assert %{
               password: ["should be at least 12 character(s)"],
               password_confirmation: ["does not match password"]
             } = errors_on(changeset)
    end

    test "validates maximum values for password for security", %{user: user} do
      too_long = String.duplicate("db", 100)

      {:error, changeset} =
        Auth.update_user_password(user, valid_user_password(), %{password: too_long})

      assert "should be at most 72 character(s)" in errors_on(changeset).password
    end

    test "validates current password", %{user: user} do
      {:error, changeset} =
        Auth.update_user_password(user, "invalid", %{password: valid_user_password()})

      assert %{current_password: ["is not valid"]} = errors_on(changeset)
    end

    test "updates the password", %{user: user} do
      {:ok, user} =
        Auth.update_user_password(user, valid_user_password(), %{
          password: "new valid password"
        })

      assert is_nil(user.password)
      assert Auth.get_user_by_email_and_password(user.email, "new valid password")
    end

    test "deletes all tokens for the given user", %{user: user} do
      _ = Auth.generate_user_session_token(user)

      {:ok, _} =
        Auth.update_user_password(user, valid_user_password(), %{
          password: "new valid password"
        })

      refute Repo.get_by(UserToken, user_id: user.id)
    end
  end

  describe "generate_user_session_token/1" do
    setup do
      %{user: user_fixture()}
    end

    test "generates a token", %{user: user} do
      token = Auth.generate_user_session_token(user)
      assert user_token = Repo.get_by(UserToken, token: token)
      assert user_token.context == "session"

      # Creating the same token for another user should fail
      assert_raise Ecto.ConstraintError, fn ->
        Repo.insert!(%UserToken{
          token: user_token.token,
          user_id: user_fixture().id,
          context: "session"
        })
      end
    end
  end

  describe "get_user_by_session_token/1" do
    setup do
      user = user_fixture()
      token = Auth.generate_user_session_token(user)
      %{user: user, token: token}
    end

    test "returns user by token", %{user: user, token: token} do
      assert session_user = Auth.get_user_by_session_token(token)
      assert session_user.id == user.id
    end

    test "does not return user for invalid token" do
      refute Auth.get_user_by_session_token("oops")
    end

    test "does not return user for expired token", %{token: token} do
      {1, nil} = Repo.update_all(UserToken, set: [inserted_at: ~N[2020-01-01 00:00:00]])
      refute Auth.get_user_by_session_token(token)
    end
  end

  describe "delete_user_session_token/1" do
    test "deletes the token" do
      user = user_fixture()
      token = Auth.generate_user_session_token(user)
      assert Auth.delete_user_session_token(token) == :ok
      refute Auth.get_user_by_session_token(token)
    end
  end

  describe "reset_user_password/2" do
    setup do
      %{user: user_fixture()}
    end

    test "validates password", %{user: user} do
      {:error, changeset} =
        Auth.reset_user_password(user, %{
          password: "not valid",
          password_confirmation: "another"
        })

      assert %{
               password: ["should be at least 12 character(s)"],
               password_confirmation: ["does not match password"]
             } = errors_on(changeset)
    end

    test "validates maximum values for password for security", %{user: user} do
      too_long = String.duplicate("db", 100)
      {:error, changeset} = Auth.reset_user_password(user, %{password: too_long})
      assert "should be at most 72 character(s)" in errors_on(changeset).password
    end

    test "updates the password", %{user: user} do
      {:ok, updated_user} = Auth.reset_user_password(user, %{password: "new valid password"})
      assert is_nil(updated_user.password)
      assert Auth.get_user_by_email_and_password(user.email, "new valid password")
    end

    test "deletes all tokens for the given user", %{user: user} do
      _ = Auth.generate_user_session_token(user)
      {:ok, _} = Auth.reset_user_password(user, %{password: "new valid password"})
      refute Repo.get_by(UserToken, user_id: user.id)
    end
  end

  describe "inspect/2 for the User module" do
    test "does not include password" do
      refute inspect(%User{password: "123456"}) =~ "password: \"123456\""
    end
  end

  describe "insert_role/2" do
    setup do
      %{user: user_fixture()}
    end

    test "inserts role", %{user: user} do
      {:ok, role} = Auth.insert_role("test_role", %{}, user)
      assert role.creator_id == user.id
      assert role.name == "test_role"
    end

    test "don't insert with unknown user", %{user: user} do
      ret = Auth.insert_role("test_role", %{}, build(:user, id: Ecto.UUID.autogenerate()))
      assert {:error, _changeset} = ret
    end
  end

  describe "insert_workspace/2" do
    setup do
      %{user: user_fixture()}
    end

    test "insert workspace", %{user: user} do
      {:ok, wp} = Auth.insert_workspace("Test workspace", user)
      assert wp.creator_id == user.id
      assert wp.name == "Test workspace"
    end

    test "don't insert with unknown user" do
      ret = Auth.insert_workspace("Test workspace", build(:user, id: Ecto.UUID.autogenerate()))
      assert {:error, _changeset} = ret
    end
  end

  describe "insert_role_membership/5" do
    setup do
      %{user: user_fixture()}
    end

    test "inserts role membership", %{user: user} do
      role = insert(:role, creator: user)
      assert {:ok, _membership} = Auth.insert_role_membership(user, role)
    end
  end

  describe "get_user_auth_context/2" do
    setup do
      user = user_fixture()
      %{user: user}
    end

    test "get simple case", %{user: user} do
      role = insert(:role, creator: user)
      membership = insert(:role_membership, %{user: user, role: role})
      context = Auth.get_user_auth_context(user)

      assert %{
               global_acl: %{},
               global_memberships: global_memberships,
               scoped_memberships: scoped_memberships
             } = context

      assert [] = scoped_memberships
    end

    test "get with scoped membership", %{user: user} do
      role = insert(:role, creator: user)
      workspace = insert(:workspace, creator: user)
      membership = insert(:role_membership, %{user: user, role: role, workspace_scope: workspace})
      context = Auth.get_user_auth_context(user)

      assert %{
               global_acl: %{},
               global_memberships: global_memberships,
               scoped_memberships: scoped_memberships
             } = context

      assert [] = global_memberships
    end

    test "get with global and scoped role", %{user: user} do
      role1 = insert(:role, %{creator: user, acl: %{test1: true, another: false}})
      role2 = insert(:role, %{creator: user, acl: %{test2: false, another2: false}})
      role = insert(:role, creator: user)
      workspace = insert(:workspace, creator: user)
      insert(:role_membership, %{user: user, role: role1})
      insert(:role_membership, %{user: user, role: role2})
      insert(:role_membership, %{user: user, role: role, workspace_scope: workspace})
      context = Auth.get_user_auth_context(user)

      assert %{
               global_acl: %{
                 "another" => false,
                 "another2" => false,
                 "test1" => true,
                 "test2" => false
               },
               global_memberships: global_memberships,
               scoped_memberships: scoped_memberships
             } = context

      refute [] == global_memberships
      refute [] == scoped_memberships
    end
  end

  describe "test workspace_hierarchy" do
    setup do
      user = user_fixture()
      %{user: user}
    end

    test "no hierarchy", %{user: user} do
      root = insert(:workspace, %{creator: user})
      assert [_root] = Auth.workspace_hierarchy(root)
    end

    test "simple hierarchy", %{user: user} do
      root = insert(:workspace, %{creator: user})
      child = insert(:workspace, %{creator: user, parent: root})
      assert [_root, _child] = Auth.workspace_hierarchy(child)
    end

    test "complex hierarchy", %{user: user} do
      root = insert(:workspace, %{creator: user, name: "root"})
      _child1 = insert(:workspace, %{creator: user, name: "1", parent: root})
      child2 = insert(:workspace, %{creator: user, name: "2", parent: root})
      child3 = insert(:workspace, %{creator: user, name: "21", parent: child2})
      assert [_root, _child2, _child3] = Auth.workspace_hierarchy(child3)
    end
  end

  describe "test create workspace authorization" do
    setup do
      user = user_fixture()
      %{user: user}
    end

    test "create workspace with dumb user", %{user: user} do
      assert {:error, :not_allowed} = Auth.create_workspace("test_workspace", user)
    end

    test "create workspace with admin user", %{user: user} do
      admin_role = insert(:role, %{creator: user, acl: %{"admin" => true}})
      insert(:role_membership, %{user: user, role: admin_role})
      assert {:ok, _workspace} = Auth.create_workspace("test_workspace", user)
    end

    test "create workspace with user owning parent workspace", %{user: user} do
      root = insert(:workspace, %{creator: user, name: "root"})
      owner_role = insert(:role, %{creator: user, acl: %{"owner" => true}})
      insert(:role_membership, %{user: user, role: owner_role, workspace_scope: root})
      assert {:ok, workspace} = Auth.create_workspace("test_workspace", user, root)
      assert workspace.parent == root
    end

    test "create workspace with user not owning parent workspace", %{user: user} do
      root = insert(:workspace, %{creator: user, name: "root"})
      owner_role = insert(:role, %{creator: user, acl: %{"owner" => false}})
      insert(:role_membership, %{user: user, role: owner_role, workspace_scope: root})
      assert {:error, :not_allowed} = Auth.create_workspace("test_workspace", user, root)
    end
  end
end
