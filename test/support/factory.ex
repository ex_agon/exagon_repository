defmodule Exagon.Repository.Factory do
  use ExMachina.Ecto, repo: Exagon.Repository.Repo

  alias Exagon.Repository.Auth.{User, Role, RoleMembership, Workspace}

  def some_user_password, do: "hello world!"

  def user_factory() do
    %User{
      email: sequence(:email, &"user-#{&1}@foo.com"),
      password: some_user_password(),
      hashed_password: Argon2.hash_pwd_salt(some_user_password())
    }
  end

  def role_factory() do
    %Role{
      name: sequence(:role_name, &"role-#{&1}"),
      creator: build(:user)
    }
  end

  def role_factory(attrs) do
    %{creator: creator} = attrs
    acl = Map.get(attrs, :acl, %{})

    %Role{
      name: sequence(:role_name, &"role-#{&1}"),
      creator: creator,
      acl: acl
    }
  end

  @spec role_membership_factory(%{:role => any, :user => any, optional(any) => any}) ::
          Exagon.Repository.Auth.RoleMembership.t()
  def role_membership_factory(attrs) do
    %{user: user, role: role} = attrs
    workspace_scope = Map.get(attrs, :workspace_scope, nil)

    %RoleMembership{
      active: true,
      valid_from: DateTime.utc_now(),
      member: user,
      role: role,
      workspace_scope: workspace_scope
    }
  end

  def workspace_factory(attrs) do
    %{creator: creator} = attrs
    parent = Map.get(attrs, :parent, nil)
    name = Map.get(attrs, :name, sequence(:wp_name, &"workspace-#{&1}"))

    %Workspace{
      name: name,
      creator: creator,
      parent: parent
    }
  end
end
