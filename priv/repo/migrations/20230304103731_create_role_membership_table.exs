defmodule Exagon.Repository.Repo.Migrations.CreateRoleMembershipTable do
  use Ecto.Migration

  def change do
    create table(:role_memberships, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :valid_from, :utc_datetime_usec
      add :valid_until, :utc_datetime_usec
      add :active, :boolean
      add :member_id, references(:users, type: :binary_id, on_delete: :delete_all), null: false
      add :role_id, references(:roles, type: :binary_id, on_delete: :delete_all), null: false
      add :workspace_scope_id, references(:workspaces, type: :binary_id, on_delete: :delete_all)

      timestamps()

    end
  end
end
