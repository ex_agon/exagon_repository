defmodule Exagon.Repository.Repo.Migrations.CreateWorkspaceTable do
  use Ecto.Migration

  def change do
    create table(:workspaces, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :creator_id, references(:users, type: :binary_id, on_delete: :delete_all), null: false
      add :parent_id, references(:workspaces, type: :binary_id, on_delete: :delete_all), null: true

      timestamps()
    end
  end
end
