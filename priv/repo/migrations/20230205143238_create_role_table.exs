defmodule Exagon.Repository.Repo.Migrations.AddRoleTable do
  use Ecto.Migration

  def change do
    create table(:roles, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :acl, :map
      add :creator_id, references(:users, type: :binary_id, on_delete: :delete_all), null: false

      timestamps()
    end

    create unique_index(:roles, [:name])
  end
end
