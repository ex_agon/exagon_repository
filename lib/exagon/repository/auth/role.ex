defmodule Exagon.Repository.Auth.Role do
  use Exagon.Repository.Schema

  alias Exagon.Repository.Auth.{User, RoleMembership, Workspace}

  @type t :: %__MODULE__{
          name: String.t(),
          acl: map(),
          creator: User.t(),
          memberships: [RoleMembership.t()]
        }

  schema "roles" do
    field(:name, :string)
    field(:acl, :map)
    belongs_to(:creator, User)
    has_many(:memberships, RoleMembership, foreign_key: :role_id)
    timestamps()
  end

  @doc false
  def create_changeset(role, attrs \\ %{}) do
    role
    |> cast(attrs, [:name, :acl, :creator_id])
    |> validate_required([:name, :acl])
    |> assoc_constraint(:creator)
  end
end
