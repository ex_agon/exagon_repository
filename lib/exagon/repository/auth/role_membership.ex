defmodule Exagon.Repository.Auth.RoleMembership do
  use Exagon.Repository.Schema
  alias Exagon.Repository.Auth.Workspace
  alias Exagon.Repository.Auth.{User, Role}

  @type t :: %__MODULE__{
          valid_from: DateTime.t(),
          valid_until: DateTime.t(),
          active: boolean(),
          member: User.t(),
          role: Role.t(),
          workspace_scope: Workspace.t()
        }

  schema "role_memberships" do
    field(:valid_from, :utc_datetime_usec)
    field(:valid_until, :utc_datetime_usec)
    field(:active, :boolean, default: true)
    belongs_to(:member, User)
    belongs_to(:role, Role)
    belongs_to(:workspace_scope, Workspace)
    timestamps()
  end

  @doc false
  def create_changeset(membership, attrs \\ %{}) do
    membership
    |> cast(attrs, [:valid_from, :valid_until, :active])
  end
end
