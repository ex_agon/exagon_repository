defmodule Exagon.Repository.Auth.Workspace do
  use Exagon.Repository.Schema

  alias Exagon.Repository.Auth.Workspace
  alias Exagon.Repository.Auth.User

  @type t :: %__MODULE__{
          name: String.t(),
          creator: User.t()
        }

  schema "workspaces" do
    field(:name, :string)
    belongs_to(:creator, User)
    belongs_to(:parent, Workspace)
    has_many(:children, Workspace)

    timestamps()
  end

  def create_changeset(wp, creator, attrs) do
    wp
    |> cast(Map.merge(attrs, %{creator_id: creator.id}), [:name, :creator_id])
    |> validate_required([:name])
    |> assoc_constraint(:creator)
  end

  @doc false
  def create_changeset(wp, creator, parent, attrs) do
    if is_nil(parent) do
      wp
      |> create_changeset(creator, attrs)
    else
      wp
      |> cast(%{parent_id: parent.id}, [:parent_id])
      |> create_changeset(creator, attrs)
      |> put_assoc(:parent, parent)
    end
  end
end
