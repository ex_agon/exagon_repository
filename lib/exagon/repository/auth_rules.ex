defmodule Exagon.Repository.AuthRules do
  alias Exagon.Repository.Auth

  def can_wreate_workspace(
        workspace,
        context
      ) do
    with true <-
           is_global_admin(context) ||
             is_parent_hierarchy_admin_or_owner(workspace, context) do
      true
    else
      _ -> false
    end
  end

  defp is_parent_hierarchy_admin_or_owner(
         workspace,
         %{
           scoped_memberships: scoped_memberships
         }
       ) do
    dbg(workspace)
    parent_ids = Auth.workspace_hierarchy(workspace.parent) |> Enum.map(fn w -> w.id end)

    Enum.filter(scoped_memberships, fn m -> m.workspace_scope.id in parent_ids end)
    |> Enum.map(fn m -> Map.get(m.role.acl, "admin", Map.get(m.role.acl, "owner", false)) end)
    |> Enum.find(fn acl -> acl end) || false
  end

  def is_global_admin(%{
        global_acl: global_acl
      }) do
    Map.get(global_acl, "admin", false)
  end
end
