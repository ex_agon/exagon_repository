defmodule Exagon.Repository.Repo do
  use Ecto.Repo,
    otp_app: :exagon_repository,
    adapter: Ecto.Adapters.Postgres
end
