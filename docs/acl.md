## Access rights
 - `admin`: 
    - global scope: the given role has admin rights (all rights allowed) on Exagon instance
    - scoped role: the given role has admin rights on the associated workspace 