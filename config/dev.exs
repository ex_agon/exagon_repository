import Config

config :exagon_repository, Exagon.Repository.Repo,
  database: "exagon_repository_repo",
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  stacktrace: true,
  show_sensitive_data_on_connection_error: true,
  pool_size: 10
