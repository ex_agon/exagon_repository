import Config

config :exagon_repository,
  ecto_repos: [Exagon.Repository.Repo]

config :exagon_repository, Exagon.Repository.Repo,
  migration_primary_key: [name: :id, type: :binary_id],
  migration_timestamps: [type: :utc_datetime_usec]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
